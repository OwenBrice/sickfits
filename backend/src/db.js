// This file connects to the remote prisma DB and gives us the ability to query it with JS
const { Prisma } = require('prisma-binding');

const db = new Prisma({
  typeDefs: 'src/generated/graphql-schema/prisma.graphql',
  endpoint: 'http://localhost:4466',
  debug: false,
});

module.exports = db;
