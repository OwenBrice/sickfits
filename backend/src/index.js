const cookieParser = require('cookie-parser');
require('dotenv').config({ path: '../variables.env' });
const createServer = require('./createServer');
const server = createServer();

// Use express middleware to handle cookies (JWT)
server.express.use(cookieParser());
// TODO Use express middlware to populate current user

server.start(
    deets => {
        console.log(`Server is running on http://localhost:${deets.port}`);
    }
);