import React, { Component } from 'react'
import { Query } from 'react-apollo'
import { ALL_ITEMS_QUERY } from '../../queries'
import Item from '../Item'
import Pagination from '../Pagination'
import { Center, ItemList } from './styles'
import { perPage } from '../../config';

class Items extends Component {
    render() {
        const { page } = this.props;
        return (
            <Center>
                <Pagination page={page} />
                <Query
                    query={ALL_ITEMS_QUERY}
                    //fetchPolicy="network-only"
                    variables={{ skip: page * perPage - perPage }}
                >
                    {({ data, error, loading }) => {
                        if (loading) return <p>Loading...</p>
                        if (error) return <p>Error: {error.message}</p>
                        return <ItemList>{data.items.map(item => <Item item={item} key={item.id} />)}</ItemList>
                    }}
                </Query>
                <Pagination page={page} />
            </Center>
        )
    }
}

export default Items;
