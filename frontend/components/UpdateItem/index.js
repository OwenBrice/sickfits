import React, { Component } from 'react'
import Router from 'next/router'
import { Query, Mutation } from 'react-apollo'
import { SINGLE_ITEM_QUERY, UPDATE_ITEM_MUTATION } from '../../queries'
import Error from '../ErrorMessage'
import Form from '../styles/Form';
import formatMoney from '../../lib/formatMoney'

class UpdateItem extends Component {
    state = {
    }

    handleChange = e => {
        const { name, type, value } = e.target;
        const val = type === 'number' ? parseFloat(value) : value;
        this.setState({ [name]: val });
    }

    handleSubmit = (updateItem) => async (e) => {
        // Stop the form from submitting
        e.preventDefault();
        // call the mutation
        console.log('Update Item', this.state);
        const res = await updateItem({
            variables: {
                id: this.props.id,
                ...this.state
            }
        });
        console.log('Updated!');
    }

    render() {
        const { id } = this.props;

        return (
            <Query query={SINGLE_ITEM_QUERY} variables={{ id }}>
                {({ data, loading }) => {
                    if (loading) return <p>Loading...</p>;
                    if (!data.item) return <p>No Item found for ID {id}</p>
                    const { title, description, image, largeImage, price } = data.item;
                    console.log('data', data);
                    return (

                        <Mutation
                            mutation={UPDATE_ITEM_MUTATION}
                        >
                            {(updateItem, { loading, error }) => (
                                <Form onSubmit={this.handleSubmit(updateItem)}>
                                    <Error error={error} />
                                    <fieldset disabled={loading} aria-busy={loading}>

                                        <label htmlFor="title">
                                            Title
                        <input
                                                type="text"
                                                id="title"
                                                name="title"
                                                placeholder="Title"
                                                required
                                                defaultValue={title}
                                                onChange={this.handleChange}
                                            />
                                        </label>

                                        <label htmlFor="price">
                                            Price
                        <input
                                                type="number"
                                                id="price"
                                                name="price"
                                                required
                                                defaultValue={price}
                                                onChange={this.handleChange}
                                            />
                                        </label>

                                        <label htmlFor="description">
                                            Description
                        <textarea
                                                id="description"
                                                name="description"
                                                placeholder="Enter a description"
                                                required
                                                rows="5"
                                                defaultValue={description}
                                                onChange={this.handleChange}
                                            />
                                        </label>

                                        <button type="submit">Sav{loading ? 'ing' : 'e'} Changes</button>
                                    </fieldset>
                                </Form>
                            )
                            }
                        </Mutation>
                    )
                }}
            </Query>
        )
    }
}

export default UpdateItem;
