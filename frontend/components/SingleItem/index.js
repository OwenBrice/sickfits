import React, { Component } from 'react'
import Head from 'next/head';
import { Query } from 'react-apollo'
import { SINGLE_ITEM_QUERY } from '../../queries'
import DisplayError from '../ErrorMessage';
import { SingleItemStyles } from './styles'

class SingleItem extends Component {
    render() {
        const { id } = this.props

        return (
            <Query query={SINGLE_ITEM_QUERY} variables={{ id }}>
                {({ data, loading, error }) => {
                    if (loading) return <p>Loading...</p>
                    if (error) return <DisplayError error={error} />
                    console.log(data);
                    if (!data.item) return <p>No Item Found for this ID {id}</p>
                    const { item } = data
                    return <SingleItemStyles>
                        <Head>
                            <title>Sick Fits | {item.title}</title>
                        </Head>
                        <img src={item.largeImage} alt={item.title} />
                        <div className="details">
                            <h2>Viewing {item.title}</h2>
                            <p>{item.description}</p>
                        </div>
                    </SingleItemStyles>
                }}
            </Query>
        )
    }
}

export default SingleItem;
