import React, { Component } from 'react'
import Router from 'next/router'
import { Mutation } from 'react-apollo'
import { CREATE_ITEM_MUTATION } from '../../queries'
import Error from '../ErrorMessage'
import Form from '../styles/Form';
import formatMoney from '../../lib/formatMoney'

class CreateItem extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        largeImage: '',
        price: 0
    }

    handleChange = e => {
        const { name, type, value } = e.target;
        const val = type === 'number' ? parseFloat(value) : value;
        this.setState({ [name]: val });
    }

    handleSubmit = (createItem) => async (e) => {
        // Stop the form from submitting
        e.preventDefault();
        // call the mutation
        console.log('state', this.state);
        const res = await createItem();
        // change the to the single item page
        console.log('res', res);
        await Router.push({
            pathname: '/item',
            query: { id: res.data.createItem.id }
        })
    }

    uploadFile = async e => {
        console.log('Uploding ...');
        const files = e.target.files;
        const data = new FormData();
        data.append('file', files[0]);
        data.append('upload_preset', 'sickfits');

        const res = await fetch('https://api.cloudinary.com/v1_1/dfovr0mjj/image/upload', {
            method: 'POST',
            body: data
        });
        const file = await res.json();
        console.log(file);
        this.setState({
            image: file.secure_url,
            largeImage: file.eager[0].secure_url
        })
    }

    render() {
        const { title, description, image, largeImage, price } = this.state;

        return (
            <Mutation
                mutation={CREATE_ITEM_MUTATION}
                variables={this.state}
            >
                {(createItem, { loading, error }) => (
                    <Form onSubmit={this.handleSubmit(createItem)}>
                        <Error error={error} />
                        <fieldset disabled={loading} aria-busy={loading}>
                            <label htmlFor="file">
                                Image
                        <input
                                    type="file"
                                    id="file"
                                    name="image"
                                    required
                                    onChange={this.uploadFile}
                                />
                                {image && <img src={image} width="200" alt="Upload Preview" />}
                            </label>

                            <label htmlFor="title">
                                Title
                        <input
                                    type="text"
                                    id="title"
                                    name="title"
                                    placeholder="Title"
                                    required
                                    value={title}
                                    onChange={this.handleChange}
                                />
                            </label>

                            <label htmlFor="price">
                                Price
                        <input
                                    type="number"
                                    id="price"
                                    name="price"
                                    required
                                    value={price}
                                    onChange={this.handleChange}
                                />
                            </label>

                            <label htmlFor="description">
                                Description
                        <textarea
                                    id="description"
                                    name="description"
                                    placeholder="Enter a description"
                                    required
                                    rows="5"
                                    value={description}
                                    onChange={this.handleChange}
                                />
                            </label>

                            <button type="submit">Submit</button>
                        </fieldset>
                    </Form>
                )
                }
            </Mutation>
        )
    }
}

export default CreateItem;
