import React, { Fragment } from 'react'
import { Query } from 'react-apollo'
import { PAGINATION_QUERY } from '../../queries'
import Head from 'next/head'
import Link from 'next/link'
import { perPage } from '../../config'
import PaginationStyles from '../styles/PaginationStyles'
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'

const Pagination = ({ page }) => {
    return (
        <Query query={PAGINATION_QUERY}>
            {({ data, loading, error }) => {
                if (loading) return <p>Loading...</p>
                const { count } = data.itemsConnection.aggregate;
                const pages = Math.ceil(count / perPage);
                return (
                    <Fragment>
                        {
                            count > 0 && (
                                <PaginationStyles>
                                    <Head>
                                        <title>
                                            Sick Fits! - Page {page} of {pages}
                                        </title>
                                    </Head>
                                    <Link
                                        prefetch
                                        href={{
                                            pathname: 'items',
                                            query: { page: page - 1 }
                                        }}>
                                        <a className="prev" aria-disabled={page <= 1}><FaArrowLeft className="reactIcons" />Prev</a>
                                    </Link>
                                    <p>Page {page} of {pages}</p>
                                    <p>{count} Items Total </p>
                                    <Link
                                        prefetch
                                        href={{
                                            pathname: 'items',
                                            query: { page: page + 1 }
                                        }}>
                                        <a className="next" aria-disabled={page >= pages}>Next<FaArrowRight className="reactIcons" /></a>
                                    </Link>
                                </PaginationStyles>
                            )
                        }
                    </Fragment>
                )
            }}
        </Query>
    )
}

export default Pagination
