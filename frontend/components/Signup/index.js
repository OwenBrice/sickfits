import React, { Component } from 'react'
import Form from '../styles/Form'
import DisplayError from '../ErrorMessage'
import { Mutation } from 'react-apollo';
import { SIGNUP_MUTATION } from '../../queries';

class Signup extends Component {
    state = {
        email: '',
        name: '',
        password: ''
    }

    saveToState = e => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    render() {
        const { email, name, password } = this.state;

        return (
            <Mutation mutation={SIGNUP_MUTATION} variables={this.state}>
                {(signup, { error, loading }) => {
                    return (
                        <Form method="post" onSubmit={e => {
                            e.preventDefault();
                            if (email.trim() && name.trim() && password.trim()) {
                                signup();
                            }
                        }}>
                            <fieldset disabled={loading} aria-busy={loading}>
                                <h2>Sign Up for An Account</h2>
                                <DisplayError error={error} />
                                <label htmlFor="email">
                                    Email
                        <input
                                        type="email"
                                        name="email"
                                        placeholder="Email"
                                        value={email}
                                        onChange={this.saveToState}
                                    />
                                </label>
                                <label htmlFor="name">
                                    Name
                        <input
                                        type="text"
                                        name="name"
                                        placeholder="Name"
                                        value={name}
                                        onChange={this.saveToState}
                                    />
                                </label>
                                <label htmlFor="password">
                                    Password
                        <input
                                        type="password"
                                        name="password"
                                        placeholder="Password"
                                        value={password}
                                        onChange={this.saveToState}
                                    />
                                </label>
                                <button type="submit">Sign Up</button>
                            </fieldset>
                        </Form>
                    )
                }}
            </Mutation>
        )
    }
}

export default Signup;
