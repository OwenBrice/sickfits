import React, { Component, Fragment } from 'react';
import Meta from '../Meta';
import Header from '../Header';
import { ThemeProvider } from 'styled-components';
import theme from '../styles/Theme';
import GlobalStyle from '../styles/GlobalStyle';
import { StyledPage, Inner } from './styles';

class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Fragment>
          <GlobalStyle />
          <StyledPage>
            <Meta />
            <Header />
            <Inner>{this.props.children}</Inner>
          </StyledPage>
        </Fragment>
      </ThemeProvider>
    );
  }
}

export default Page;
