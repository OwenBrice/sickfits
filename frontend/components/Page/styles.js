import styled from 'styled-components';

const StyledPage = styled.div`
  background: ${props => props.theme.white};
  color: ${props => props.theme.black};
`;

const Inner = styled.div`
  max-width: ${props => props.theme.maxWith};
  margin: 0 auto;
  padding: 2em;
`;

export { StyledPage, Inner };
