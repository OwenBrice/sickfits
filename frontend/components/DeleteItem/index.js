import React, { Component } from 'react'
import { Mutation } from 'react-apollo'
import { DELETE_ITEM_MUTATION, ALL_ITEMS_QUERY } from '../../queries'
import { FaTrash } from 'react-icons/fa'

class DeleteItem extends Component {
    update = (cache, payload) => {
        // Manually update the cache on the client, so it matches the server
        // 1. Read the cache for the items we want
        const data = cache.readQuery({ query: ALL_ITEMS_QUERY });
        console.log(data, payload);
        // 2. Filter the deleted item out of page
        data.items = data.items.filter(item => item.id !== payload.data.deleteItem.id);
        // 3. Put the items back!
        cache.writeQuery({ query: ALL_ITEMS_QUERY, data });
    }

    render() {
        const { id } = this.props;

        return (
            <Mutation
                mutation={DELETE_ITEM_MUTATION}
                variables={{ id }}
                update={this.update}
            >
                {(deleteItem, { error }) => (
                    <button
                        onClick={() => {
                            if (confirm('Are you sure you want to delete this item?')) {
                                deleteItem();
                            }
                        }}
                    >Delete Item<FaTrash className="reactIcons" /></button>
                )}
            </Mutation>
        )
    }
}

export default DeleteItem;
